from __future__ import print_function, division
import math

import sympy

import matrices


def f_kin_ref(theta1, theta2, theta3, symbolic=False):
    """Reference fonction for forward model. Expects degrees."""
    return f_kin_rad(sympy.pi * theta1 / 180,
                     sympy.pi * theta2 / 180,
                     sympy.pi * theta3 / 180,
                     symbolic=symbolic)

def f_kin_rad(theta1=sympy.Symbol('theta1'),
              theta2=sympy.Symbol('theta2'),
              theta3=sympy.Symbol('theta3'),
              symbolic=False):
    """Radians version of f_kin_ref."""
    constants = {sympy.pi: math.pi}

    # base to first joint
    r0 = sympy.Symbol('r0')
    constants[r0] = 5.5 + 36
    hm0 = matrices.HomMatrix(0, r0, 0, 0)

    # first to second joint
    d1     = sympy.Symbol('d1')
    alpha1 = sympy.pi/2
    r1     = sympy.Symbol('r1')
    constants[d1] = -14.5
    constants[r1] = 26 + 23
    hm1 = matrices.HomMatrix(alpha1, r1, d1, theta1)

    # second to third joint
    beta2  = sympy.Symbol('beta2')
    d2     = 0
    alpha2 = 0
    r2     = sympy.Symbol('r2')
    constants[beta2] = -math.atan2(8+8+6.5, 36+5.5+5.5+13)
    constants[r2]    =  math.sqrt((8+8+6.5)**2 + (36+5.5+5.5+13)**2)
    hm2 = matrices.HomMatrix(alpha2, r2, d2, -theta2 + beta2)

    # third joint to tip
    beta3  = sympy.Symbol('beta3')
    d3     = 0
    alpha3 = 0
    r3     = sympy.Symbol('r3')
    constants[beta3] = math.atan2(8+5.5, 52+41)
    constants[r3]    = math.sqrt((8+5.5)**2 + (52 + 41)**2)
    hm3 = matrices.HomMatrix(alpha3, r3, d3, theta3 - beta2 - sympy.pi/2 + beta3)

    m = sympy.Matrix([0, 0, 0, 1])

    res = hm0.m*hm1.m*hm2.m*hm3.m*m
    if not symbolic:
        res = res.subs(constants)
        return res[0].evalf(), res[1].evalf(), res[2].evalf()
    else:
        return res[0], res[1], res[2]
