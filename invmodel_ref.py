#!/usr/bin/python
# -*- coding: utf-8 -*-

import sympy as sp
import math
import copy

from fwdmodel_ref import f_kin_rad

EPSILON = 5

def i_kin_2d(alpha, beta, d_a, d_b):

#    phi_a, phi_b = sp.symbols('phi_a, phi_b', real = True)

    cos_phi_b = (alpha ** 2 + beta ** 2 - d_a ** 2 - d_b ** 2) / (2 * d_a * d_b)
    # sin_phi_b = [ - sp.sqrt(1 - cos_phi_b ** 2), sp.sqrt(1 - cos_phi_b ** 2)]
    solutions_phi = []

    for choice_b in [-1, 1]: # sin_phi_b = [ - sp.sqrt(1 - cos_phi_b ** 2), sp.sqrt(1 - cos_phi_b ** 2)]
        sin_phi_b = choice_b*sp.sqrt(1 - cos_phi_b**2)

        # phi_b = [sp.atan2(spb, cos_phi_b) for spb in sin_phi_b]
        phi_b = sp.atan2(sin_phi_b, cos_phi_b)

        cos_phi_a = (alpha * (d_a + d_b * cos_phi_b) + beta * d_b * sin_phi_b) / (alpha ** 2 + beta ** 2)

        for choice_a in [-1, 1]:
            sin_phi_a =  choice_a * sp.sqrt(1 - cos_phi_a ** 2)
            phi_a = sp.atan2(sin_phi_a, cos_phi_a)

            solutions_phi.append((phi_a, phi_b))

    return solutions_phi

def i_kin_symbolic():

    symbolic_solutions = []

    #define the symbols
    theta1, theta2, theta3 = sp.symbols('theta1, theta2, theta3', real=True)
    x, y, z = sp.symbols('x, y, z', real=True)

    #some constant depending on the geometry
    h = sp.symbols('h', real=True)

    #first let's considere theta1 in the (x,y) plan
    theta1_ = sp.atan2(y, x - h)

    for theta1 in [theta1_, theta1_ + sp.pi]:

        #input for the 2D problem
        u = sp.symbols('u', real=True)
        t = sp.symbols('t', real=True)
        gamma2 = sp.symbols('gamma2', real=True)
        gamma3 = sp.symbols('gamma3', real=True)

        x_p   = (x - h) * sp.cos( - theta1) - y * sp.sin( - theta1)
        alpha = x_p - u
        beta  = z   - t

        d_a, d_b = sp.symbols('d_a, d_b', real=True)
        solutions_phi = i_kin_2d(alpha, beta, d_a, d_b)

        phi_solutions = [(theta1, phi_a - gamma2, phi_b + gamma3) for phi_a, phi_b in solutions_phi]
        symbolic_solutions.extend(phi_solutions)

    symbols = {'h': h, 'u': u, 't': t, 'gamma2': gamma2, 'gamma3': gamma3,
               'd_a': d_a, 'd_b': d_b,
               'x': x, 'y': y, 'z': z}

    return symbolic_solutions, symbols

# those are generated once
symbolic_solutions, symbols = i_kin_symbolic()

def i_kin_constants():

    constants = {symbols['h']      :  41.5,
                 symbols['u']      :  49.0,
                 symbols['t']      : -14.5,
                 symbols['gamma2'] :  math.atan2(8+8+6.5, 36+5.5+5.5+13),
                 symbols['gamma3'] :  math.atan2(52+41, 8+5.5) - math.atan2(8+8+6.5, 36+5.5+5.5+13),
                 symbols['d_a']    :  math.sqrt((8+8+6.5)**2 + (36+5.5+5.5+13)**2),
                 symbols['d_b']    :  math.sqrt((8+5.5)**2 + (52 + 41)**2),
                }

    cst_solutions = [(theta1.subs(constants).evalf(),
                      theta2.subs(constants).evalf(),
                      theta3.subs(constants).evalf()) for theta1, theta2, theta3 in symbolic_solutions]

    return cst_solutions

constants_solutions = i_kin_constants()

def i_kin_rad(xd, yd, zd):

    #numerical application
    pos = {symbols['x']: xd,
           symbols['y']: yd,
           symbols['z']: zd}

    num_solutions = [(theta1.subs(pos).evalf(),
                      theta2.subs(pos).evalf(),
                      theta3.subs(pos).evalf()) for theta1, theta2, theta3 in copy.deepcopy(constants_solutions)]

    #filtering
    filtered_solutions = []
    for t1, t2, t3 in num_solutions:
        if sp.im(t1) == sp.im(t2) == sp.im(t3) == 0:
            t1, t2, t3 = sp.re(t1), sp.re(t2), sp.re(t3)
            if sum((x1_i - x2_i)**2 for x1_i, x2_i in zip(f_kin_rad(t1, t2, t3), (xd, yd, zd))) < EPSILON:
                filtered_solutions.append((t1 % (2*math.pi), t2, t3))

    return filtered_solutions

def i_kin_ref(xd, yd, zd):
    return [(math.degrees(t1), math.degrees(t2), math.degrees(t3)) for t1, t2, t3 in i_kin_rad(xd, yd, zd)]

if __name__ == '__main__':
    print(i_kin_ref(164, 0, -130))
